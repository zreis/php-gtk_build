PHP 5.4
=======

PHP build steps taken from [https://wiki.php.net/internals/windows/stepbystepbuild](https://wiki.php.net/internals/windows/stepbystepbuild)

## Set-Up Build Enviorment

#### Windows Version
Build Enviroment Supported Operating Systems:

Windows 7, Windows Server 2003 R2 Standard Edition (32-bit x86), Windows Server 2003 R2 Standard x64 Edition , Windows Server 2008,
Windows Server 2008 R2, Windows Vista, Windows XP Service Pack 3.

#### Install a Compiler
Although not officially supported or documented by the PHP Team you may install Microsoft Visual C++ 2010 Express (`VS2010Express1.iso`)

**Be sure to install all windows updates & install ('VS2010Express SP1') from windows update.**

*If you like to play by the rules - the offical method for PHP < 5.0 requires you to install Microsoft VisualC++ 2008 Express (VS2008ExpressWithSP1ENUX1504728.iso)

Install Microsoft Windows SDK 6.1 (6.0.6001.18000.367-KRMSDK_EN.iso)*

#### **Special Enviorment Set-Up Work Arounds for Visual Studio 2010 Express:**
Although not explicitly required you may install the Windows SDK 7.1. 

By default the Windows SDK 7.1 install will not properly register within Visual Studio C++ 2010 Express. So the `setenv` command will not work.

First goto Control Panel -> Add/Remove Programs and unistall the `Microsoft Visual C++ 2010 Redistributable - 10.0.4219` both `x86` & `x64`.

Install Microsoft Windows SDK 7.1 (`http://www.microsoft.com/en-us/download/details.aspx?id=8279`), selecting the defaults and adding the compilers.

Check Windows Updates and install them.

Additionally you may need to restore the Visual C++ compilers and libraries that may have been removed when Visual Studio 2010 Service Pack 1 (SP1) was installed.

Download & install `VC-Compiler-KB2519277.exe` from [here](http://www.microsoft.com/en-us/download/details.aspx?id=4422).

Lastly we need to ensure that the correct compiler is registered on the command line if using the VS2010 command prompt.
Open `C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\Tools\VCVarsQueryRegistry.bat` with a text-editor and make sure that line 25 & 26 read:

    :GetWindowsSdkDirHelper
    @for /F "tokens=1,2*" %%i in ('reg query "%1\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.1" /v "InstallationFolder"') DO (

if not, then be copy the code above and replace the content, save and close the file.

## PHP + php_cairo

#### 01)
create the directory `c:\php-sdk`

#### 02)
unpack the binary-tools.zip archive (`php-sdk-binary-tools-20110915.zip`) from 
[http://windows.php.net/downloads/php-sdk/](http://windows.php.net/downloads/php-sdk/)
into this directory, there should be one sub-directory called `bin` and one 
called `script`

**Visual Studio 2010 TIP**
Open `c:\php-sdk\bin\phpsdk_buildtree.bat` with a text-editor. Add the following under this line: 

    MD %_%\vc9\x64\deps\include
    
add the following:

    MD %_%\vc10\x86\deps\bin
    MD %_%\vc10\x86\deps\lib
    MD %_%\vc10\x86\deps\include
    MD %_%\vc10\x64\deps\bin
    MD %_%\vc10\x64\deps\lib
    MD %_%\vc10\x64\deps\include   

save and close the file.

#### 03)
open the windows sdk 7.1 shell (Start -> All Programs -> Microsoft Windows SDK v7.1
-> CMD Shell) and execute the following commands in it:

    setenv /x86 /win7 /release
    cd c:\php-sdk\
    bin\phpsdk_setvars.bat
    bin\phpsdk_buildtree.bat php54

#### 04)
download and extract php sources (currently version is `php-5.4.10-src.zip`) to
`C:\php-sdk\php54\vc9\x86` so that the following directory gets created:
`C:\php-sdk\php54\vc9\x86\php-5.4.10-src`

#### 05)
download the prepackaged deps library `deps-5.4-vc9-x86.7z` from 
[http://windows.php.net/downloads/php-sdk/](http://windows.php.net/downloads/php-sdk/)
and extract to `C:\php-sdk\php5\vc9\x86` (merge with existing `deps` directory)
The deps directory in `C:\php-sdk\php54\vc9\x86` should contain all required libraries
(see [http://wiki.php.net/internals/windows/libs](http://wiki.php.net/internals/windows/libs)).

#### 06)
download php_cairo source (I used 
[https://github.com/gtkforphp/cairo](https://github.com/gtkforphp/cairo) 
commit: df45aa1418) and extract to `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\ext` and rename 
extracted directory to `cairo`

#### 07)
extract `gtk+-bundle_2.24.10-20120208_win32.zip` in `c:\gtk` (download from 
[http://ftp.gnome.org/pub/gnome/binaries/win32/gtk+/2.24/](http://ftp.gnome.org/pub/gnome/binaries/win32/gtk+/2.24/))

#### 08)
Copy overwriting existing files:

    c:\gtk\include\cairo               to  C:\php-sdk\php54\vc9\x86\deps\include\cairo
    c:\gtk\include\fontconfig          to  C:\php-sdk\php54\vc9\x86\deps\include\fontconfig
    c:\gtk\include\freetype2\freetype  to  C:\php-sdk\php54\vc9\x86\deps\include\freetype
    c:\gtk\include\ft2build.h          to  C:\php-sdk\php54\vc9\x86\deps\include\ft2build.h

copy the following files from `c:\gtk\lib\`  into  `C:\php-sdk\php54\vc9\x86\deps\lib\`

    cairo.def
    cairo.lib
    fontconfig.def
    fontconfig.lib
    freetype.def
    freetype.lib
    libcairo.dll.a
    libfontconfig.dll.a
    libfreetype.dll.a
 
#### 09)
Open `C:\php-sdk\php54\vc9\x86\deps\include\fontconfig\fontconfig.h`
with a text-editor. Find the line that says:

    #include <unistd.h>

and replace with:
    
    #ifdef PHP_WIN32
    # include "win32/unistd.h"
    #else
    # include <unistd.h>
    #endif    

#### 10)
run in the windows-sdk-shell:

    cd C:\php-sdk\php54\vc9\x86\php-5.4.10-src
    buildconf
    configure --with-gd --enable-cli --disable-zts --enable-cli-win32 --with-cairo=shared
    nmake

#### 11)
Copy `php_cairo.dll` from `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release` to
    `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release\ext`

copy the following files from `C:\gtk\bin` to `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release`

    libcairo-2.dll
    libexpat-1.dll
    libfontconfig-1.dll
    freetype6.dll
    libpng14-14.dll
    zlib1.dll
       

#### 12)
Create a `php.ini` in `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release` with 
the following content:

    extension_dir=.\ext
    extension=php_cairo.dll

#### 13)
test the newly built PHP

    cd C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release
    php -v
    php -m


## php_gtk2


#### 01)
get `grep` and `sed` for windows (for example: 
[http://unxutils.sourceforge.net/UnxUtils.zip](http://unxutils.sourceforge.net/UnxUtils.zip))
and put them in the `PATH` (ex. `c:\windows`)

#### 02)
extract php-gtk sources to `c:\php-gtk` (I used 
[https://github.com/auroraeosrose/php-gtk-src](https://github.com/auroraeosrose/php-gtk-src)
commit: e972b2524a)
                
#### 03)
We need a `php.exe`, so add `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release` to 
`PATH` in `C:\Program Files\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat`

#### 04)
In  `C:\Program Files\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat`

    add to INCLUDE  C:\php-sdk\php54\vc9\x86\php-5.4.10-src
    add to INCLUDE  C:\php-sdk\php54\vc9\x86\deps\include
    add to LIB      C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release
    add to LIB      C:\php-sdk\php54\vc9\x86\deps\lib

#### 05)
    mkdir c:\php_build

#### 06)
extract
[http://www.php.net/extra/win32build.zip](http://www.php.net/extra/win32build.zip)
into `c:\php_build`
                              
#### 07)
copy from `c:\gtk` to `c:\php_build`

    include\cairo                     -> include\cairo
    include\gtk-2.0\*                 -> include\*
    include\glib-2.0\*                -> include\*
    include\atk-1.0\atk               -> include\atk
    include\pango-1.0\pango           -> include\pango
    include\gdk-pixbuf-2.0\gdk-pixbuf -> include\gdk-pixbuf
    include\fontconfig                -> include\fontconfig
    include\freetype2\freetype        -> include\freetype

    lib\*  -> lib\*

Overwrite conflicting files

#### 08)
copy 

    c:\php_build\lib\glib-2.0\include\*   -> c:\php_build\include\*
    c:\php_build\lib\gtk-2.0\include\*    -> c:\php_build\include\*

#### 09)
In  `C:\Program Files\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat`

    add to INCLUDE  C:\php_build\include
    add to LIB      C:\php_build\lib
and execute the batch, in the windows sdk shell, to activate the changes


#### 10) ***** HACK *****
copy `C:\Program Files\Microsoft SDKs\Windows\v6.1\Samples\winui\TSF\tsfapp\winres.h`
to `c:\php_build\include`

#### 11)
copy `php_cairo_api.h` and  `php_cairo.h` 
from `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\ext\cairo` to `c:\php_build\include`

#### 12)
run in the windows-sdk-shell:

    cd C:\php-gtk
    buildconf
    configure --with-php-build=..\php_build --disable-zts --enable-gd
    nmake

#### 13)
copy `C:\php-gtk\Release\php_gtk2.dll` to
`C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release\ext\php_gtk2.dll`

copy the following files from `C:\gtk\bin`
to `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release`

    libgtk-win32-2.0-0.dll
    libgdk-win32-2.0-0.dll
    libgdk_pixbuf-2.0-0.dll
    intl.dll
    libgio-2.0-0.dll
    libglib-2.0-0.dll
    libgmodule-2.0-0.dll
    libgobject-2.0-0.dll
    libgthread-2.0-0.dll
    libpango-1.0-0.dll
    libpangocairo-1.0-0.dll
    libpangoft2-1.0-0.dll
    libpangowin32-1.0-0.dll
    libatk-1.0-0.dll

#### 14)
add the following line to `C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release\php.ini`

    extension=php_gtk2.dll 

#### 15)
verify that the extension is loaded

    cd C:\php-sdk\php54\vc9\x86\php-5.4.10-src\Release
    php -m


## DONE!